---
title: Entities, DAO and Database
template: main-repo.html
---

Let's define the Entities, DAO and Database using Room.

Here's our schema

```mermaid
erDiagram
    RATING ||--o{ MOVIE: movies
    MOVIE ||--o{ ROLE : cast
    ROLE }o--|| ACTOR : appearance
```

What does this mean?

   * RATING <-> MOVIE is a one-to-many relationship

      * A RATING can be associated with zero or more MOVIEs
      * A MOVIE has exactly one RATING

   * MOVIE <-> ACTOR is a many-to-many relationship

      * An ACTOR can appear in zero or more MOVIEs
      * A MOVIE can cast zero or more ACTORs
      * To implement this, we introduce an associative entity ROLE with a one-to-many relationship on each side
         * A MOVIE can cast zero or more ROLEs
         * An ACTOR can appear in zero or more ROLEs
         * A ROLE can appear in exactly one MOVIE
         * A ROLE is played by exactly one ACTOR

Let's look at the attributes of each entity. All entities are represented as Kotlin data classes.

## Rating

Represents an MPAA rating (G, PG, PG-13, R, NR)

| Attribute   | Type   | Key |
|-------------|--------|-----|
| id          | String | PK  |
| name        | String |     |
| description | String |     |

## Actor

An actor that can appear in movies

| Attribute | Type   | Key | Comment |
|-----------|--------|-----|---------|
| id        | String | PK  |         |
| name      | String |     |         |

## Movie

A movie

| Attribute   | Type   | Key |
|-------------|--------|-----|
| id          | String | PK  |
| title       | String |     |
| description | String |     |
| ratingId    | String | FK  |

## Role

Association entity that casts actors into movies

| Attribute      | Type   | Key |
|----------------|--------|-----|
| movieId        | String | FK  |
| actorId        | String | FK  |
| character      | String |     |
| orderInCredits | int    |     |

## MovieDAO

We start with a DAO that has basic CRUD (Create, Read, Update, Delete) operations. Later we're going to add a concrete function, so I chose to make the DAO an abstract class rather than an interface.

All one-shot functions are declared as `suspend` functions so they'll ensure they're not run on the UI thread.

The query functions immediately return a `Flow` that we'll collect inside a coroutine.

We'll add a few more functions as the example grows.

## MovieDatabase

A very typical Room database declaration. The database will contain Movie, Actor, Role and Rating entities and expose a MovieDAO.

## DatabaseBuilder.kt

This Kotlin file hosts a single function that we'll use to create an instance of the database. Defining this function here, in the `data` module, avoids the need to make the `repository` module depend on Room.

Note that if you want to see the SQL queries that are being run, you can uncomment the `setQueryCallback` call and look at the **Logcat** view at the bottom of Android Studio.

