---
title: Add Data/Repository Modules
template: main-repo.html
---

We created new `data` and `repository` modules. To do this:

   1. Right-click the top-level project in the **Project** view 
   2. Choose **New -> Module**

      ![Choosing New Module](screenshots/create-data-module-1.png)

   3. Select the **Android Library** template
   4. Enter `data` as the **Application/Library name**
   5. Update the **Package name** to _lastname_._firstname_._appname_.data

      ![Choosing New Module](screenshots/create-data-module-2.png)

   6. Press **Finish**
   7. Press **Add** to add the newly-created files to git

      ![Choosing New Module](screenshots/create-data-module-3.png)


Then do the same for the `repository` module.

Note that `settings.gradle` has been updated to add the two new modules. This is how gradle (and Android Studio) know that these are modules it needs to build.