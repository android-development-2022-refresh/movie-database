---
title: Project Setup
template: main-repo.html
---

We start with the "Empty Compose Activity" project wizard.

Update the Android, Kotlin and dependency versions to the latest available.

After updating the files, you'll need to re-synchronize them with Android Studio. Android Studio reads the build scripts to determine which modules and which dependencies are used so it can provide code-assist and lint checks in the IDE.

When you change a build script, Android Studio will normally display a banner at the top of the file indicating that it needs to be re-synchronized, and you can click "Sync Now" to do so. Otherwise you can click the elephant icon on the toolbar to perform this synchronization.

![Gradle Synchronization](screenshots/sync-1.png)