---
title: Add Room
template: main-repo.html
---

First, I did a little cleanup on the new build scripts, updating versions.

Now we're adding the Room dependencies and the Kotlin Symbol Processor (KSP).

The Kotlin Symbol Processor is a compiler plugin that loads "symbol processors".

When we specify

```groovy
dependencies {
   ...
   ksp "androidx.room:room-compiler:$room_version"
   ...
}
```

in our `build.gradle` build script for the `data` module, we're loading in the "Room Compiler" symbol processor. This runs as part of the overall Kotlin compilation process, and looks for Room annotations like `@Entity` and `@Database`. When it sees them, it generates code to perform our database work.

We're only using Room in the `data` module, so we don't need to add these dependencies in the `repository` module's `build.gradle` file.

Remember after changing any gradle build files to re-synchronize!