---
title: Module Dependencies
template: main-repo.html
---

Now we need to tell the modules how they depend on each other.

```mermaid
flowchart LR
    subgraph User Interface Layer
    ui[Module: app\n]
    end
    ui --> repo
    subgraph Data Layer
    repo[Module: repository]
    ds[Module: data]
    repo --> ds
    end
```

We set up these dependencies by using `implementation project(":xyz")` in our dependencies.

Dependencies can be specified as _implementation_ dependencies or _api_ dependencies. 

Implementation dependencies are only available within the module that declares them. API dependencies become part of the API of the current module and are made available to any module that depends on the current module.

Normally, you should choose the _implementation_ dependency type so modules can hide their implementation dependencies. This also improves build speed, as changes to implementation dependencies are not inherited, and won't trigger a rebuild of modules that depend on the current module.