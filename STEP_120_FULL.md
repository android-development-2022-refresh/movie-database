---
title: Wrap-Up
template: main-full.html
---

We'll stop here for now and flesh this example out more over time. We'll need to fill in all of the details for displaying `Movies` and `Actors` and create a nice user interface.

For now, we have enough of an example to create your first real assignment!