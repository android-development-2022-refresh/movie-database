---
title: Refactoring
template: main-repo.html
---

That `MainActivity` has become quite the mess. While it can be tempting to start off throwing all of your `@Composable` functions into the same file, try to avoid that temptation, as you'll quickly end up with our current mess.

In this step, I've moved the functions out into their own files. `SimpleButton` and `SimpleText` have been moved to a `components` package, and the other `@Composable` functions moved into a `screens` package.

Much better now!