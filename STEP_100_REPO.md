---
title: Rating with Movies
template: main-repo.html
---

Let's create a screen to display a Rating and a list of its Movies. This screen will be displayed when a `Rating` is clicked in the rating list screen. We'll keep track of the `id` of the clicked `Rating` in a new `Screen` object, passing that `id` to the new display screen.

When we set up the real UI later on, we'll be tracking screens that the user has visited in a stack, so they can go back. This allows the user to explore movie and actor relationships. If the user is watching a "The Transporter" movie and views its cast in this application, they might want to see what other movies star "Jason Statham". After looking, they may want to go back to see who else is in "The Transporter" by pressing the Android "back" button.

If we keep track of the _actual_ Dtos in that stack, they could become stale. The user could have edited a `Movie` or `Actor`, and the old DTO instances would still old the old information. Going back would show the stale data.

Instead, we track the `ids` of the objects we've seen on the stack.

But how do we get the data we need to display on the screen? We pass the id _and_ a function that can fetch the actual data. When we enter the screen, it will kick off a coroutine to fetch the data, and update itself when the fetch has completed.

There are several files being modified in this step; we'll work our way from the `data` module up through the `app` module a piece at a time.

## `data` Rating.kt

`Rating` <-> `Movie` is a one-to-many relationship. We can use a non-`@Entity` Kotlin data class to tell Room to fetch the `Rating` and its `Movies` at the same time.

First, we create a helper class, `RatingWithMovies` to ask Room to fetch the `Rating` and its `Movies`. We'll be asking the DAO to fetch a single Rating _by its id_. We'll hold onto it in the `@Embedded rating` property. Room sees the `@Relation` annotation on the `movies` property and runs a query to fetch all `Movies` that have a `ratingId` value that matches the `Rating id`.

## `data` MovieDAO.kt

We add a new `@Query` to the DAO to look up a single `Rating` by its `id`. Note the return type! We're _not_ returning an entity here; we're returning the new `RatingWithMovies` data class. Because it contains a `@Relation` annotation, Room will need to run an additional query to fetch the `Movies`. To ensure we're seeing consistent data, we must annotate `getRatingWithMovies` with `@Transaction` so no database updates can occur between the `Rating` query and the query to get its `Movies`.

## `repository` Rating.kt

Moving up to the `repository` module, we create a corresponding **Data Transfer Object** `RatingWithMoviesDto` and a `toDto` function to make the transform easy.

## `repository` MovieRepository, MovieDatabaseRepository

The screen will be fetching the `Rating` by its id and using its result. We can't do this on the UI thread or the user interface can become less responsive and appear "janky". So the screen will kick off a coroutine to fetch the `Rating` and its `Movies`.

We expose the fetching functionality through the `MovieRepository` as a one-shot `suspend` function (which in turn will be exposed through the `MovieViewModel` for the UI to call).

## `app` MovieViewModel

Because we're adding a new `Screen`, we need to create data to represent it. The "rating screen" needs to know which `Rating` to display, so we cannot use a singleton `object`; we'll need separate instances for each time we see a rating screen.

We define a Kotlin data class called `RatingScreen` (implementing `Screen`) to track this.

To facilitate the `Rating` lookup, we define a passthrough `getRatingWithMovies` function, which asks the `repository` module to fetch the data (which in turn asks the `data` module to fetch the data.)

## `app` MainActivity.kt

(This file is getting big and managing too many different things... we'll clean that up in the next step.)

Let's look at each of the changed pieces.

### SimpleText and SimpleButton

I'm tweaking these functions to allow text to be clicked (so we can click a `Rating` and switch to the rating display screen). The `.clickable` modifier handles this. Unfortunately I had used `SimpleText` inside of a `Button`, and this change would override the button-click functionality! So I converted the text display in the button back to Compose's `Text`.

### Screen Selection

The `when` expression used to determine what to display on the screen now includes the `RatingScreen`. We need to check for it using `is RatingScreen`, which checks to see if the screen is an _instance_ of `RatingScreen`, rather than comparing it to a singleton `object` value as we did for the lists.

We also add a "what to do when the name of a `Rating` is clicked on the `RatingList`" lambda to the `RatingList` function.

### Rating Display

The `RatingDisplay` function has some interesting features that we'll touch upon here and cover in more detail when we talk about Compose.

First, the parameters:

   * We pass in the `id` of the `Rating` we want to display.
   * We need to look up the `Rating` (and its `Movies`). To perform the lookup, we pass in a `fetchRatingsWithMovies` function.

     ```kotlin
     fetchRatingWithMovies: suspend (String) -> RatingWithMoviesDto
     ```

     This function takes a `String` (the rating id) as a parameter, and returns a `RatingWithMoviesDto`. Because it will need to call `suspend` functions to perform this one-shot fetch, this function must also be a `suspend` function.

     You'll get used to function-type syntax like this pretty quickly; you'll see it a lot. Whenever you see `->` inside a parameter type, you know a function is being passed in. The parameter list for that function appears in `(...)` and the return type appears after the `->`.

     If we pass in a function that doesn't return anything, we'll use `Unit` as the return type. `Unit` means "just perform some processing; don't return a value".

Next, you'll see 

```kotlin
var ratingWithMoviesDto by remember { mutableStateOf<RatingWithMoviesDto?>(null) }
```

There's a _lot_ going on there, and we'll cover it in detail later. For right now, think of this as a fancy way to declare a local variable in Compose, conceptually similar to 

```kotlin
var ratingWithMoviesDto: RatingWithMoviesDto = null
```

This variable is used as the data that the rest of the function displays on the UI. But we need to fetch that data.

To do this, we launch a coroutine and set that local variable:

```kotlin
LaunchedEffect(key1 = ratingId) {
    // starts a coroutine to fetch the rating
    ratingWithMoviesDto = fetchRatingWithMovies(ratingId)
}
```

A coroutine is launched whenever the passed-in `ratingId` value changes (if the last rating was still being fetched, that coroutine will be canceled).

Finally, we create the UI. We use Kotlin's `let` scoping function to only display the data if a rating has been fetched. By writing

```kotlin
ratingWithMoviesDto?.let { ratingWithMovies ->
    ...
}
```

We check if `ratingWithMoviesDto` is non-null, and if so, call `let` passing in that non-null value. Keep in mind that `ratingWithMoviesDto` is set by a coroutine, and could change at any time; using this `let` call will capture its value so we have a consistent value to work with in the `let` body.

