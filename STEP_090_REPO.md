---
title: Hideous UI
template: main-repo.html
---

Let's see if anything is working by creating a truly hideous (but simple) UI. (When we talk about Jetpack Compose in more detail, we'll create much better UI.)

## build.gradle

First, we'll need to access the view model in the user interface. We update the `app` module's `build.gradle` to include the `lifecycle-viewmodel-compose` dependency. This gives us a `viewModel()` function that we can use as a default parameter value when we're defining our UI functions.

!!! info

    Super quick intro to Jetpack Compose: You write functions to declare what the UI should look like. When the values of the parameters change, the UI is refreshed. (That's a huge oversimplification, but should be enough to work with for now.)

    Notice the word "declare"? These functions _describe_ the user interface rather than creating it directly. Behind the scenes, that description is updated as these functions run, and the Compose runtime determines what has changed for each update so it can refresh the UI.

    Follow this example when writing your assignment and you'll survive just fine. Later we'll detail how things work. Unfortunately, if we spent the time to learn Compose _and_ Room before starting the first assignment, we'd have several weeks of no assignments...

## MovieViewModel

For our simple UI, we need some way to track which "screen" the user is viewing. We do that be defining a `sealed interface` named `Screen`. This is a special type of interface that requires all implementations be defined _in the same module_. This is awesome, because the compiler can know _all possible values_, and we can write exhaustive `when` expressions!

At this stage we use `objects` to represent each screen the user might visit. Kotlin's `object` keyword defines a _singleton_ instance that implements the interface. Right now there's no data needed inside the screen state, so `objects` work perfectly. We'll add a screen that views a specific movie shortly, and this will require a class that we can instantiate.

We expose the current screen using Jetpack Compose state. (I'm waiving my hands furiously right now) Just think of it as a box that can store data, and Compose will know when that data changes so it can refresh the UI.

## MainActivity

Finally! The user interface! We define functions annotated with `@Composable` (known as "Composable Functions") to declare parts of our user interface. 

!!! note

    The names of these `@Composable` functions will probably look odd. Jetpack Compose uses an `UpperCamelCase` naming convention for `@Composable` functions that declare parts of the UI.

![Hideous UI](screenshots/hideous-ui.png)

We're defining a (did I mention hideous?) UI that has buttons at the top of the screen to switch between screens and reset the database. You'll need to reset the database to see _any_ data.

!!! warning

    When choosing types via content selection, be careful which package you select. `Modifier` is particularly painful, as the content assist often sorts `java.lang.reflect.Modifier` above `androidx.compose.ui.Modifier` (which is the one we want when using Compose). I can't count the number of times I've accidentally selected `java.lang.reflect.Modifier`...

    This mistake will usually become obvious when you try to use other functions (and they're not found). For example, when writing these samples I made the `java.lang.reflect.Modifier` mistake, and then `padding` and `clickable` couldn't be found.
