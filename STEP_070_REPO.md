---
title: Repository Types
template: main-repo.html
---

Now the basics of the `repository` module.

## DTOs

We create Data Transfer Objects to abstract and restrict how the data is used. These DTOs are immutable, which will help Jetpack Compose optimize UI updates.

We also define extension functions to convert between the entities defined in the `data` module and the DTOs we expose from this `repository` module.

Note that the extension functions are marked `internal`. This makes them accessible anywhere inside the `repository` module, but **not** outside the module.

## MovieRepository

The `MovieRepository` interface defines how we communicate with a repository. This allows different repository implementations (later we'll add a web-service implementation). Note that I'm _not_ using `vararg` in the `MovieRepository`. This is because it simplifies our web services implementation when we do it later. 

## MovieDatabaseRepository

This is the concrete implementation of `MovieRepository` that we use to work with the Room database.

Much of it (the one-shot functions) is direct passthrough to the DAO. The query functions that expose a `Flow` transform the returned entities into DTOs. The `map` function on `Flow` creates a new flow that calls the nested `map` on the list of entities to convert them into DTOs.