package com.javadude.movies.screens

import androidx.compose.runtime.Composable
import com.javadude.movies.components.SimpleText
import com.javadude.movies.repository.ActorDto

@Composable
fun ActorList(
    actors: List<ActorDto>
) {
    SimpleText(text = "Actors")
    actors.forEach {
        SimpleText(text = it.name)
    }
}
