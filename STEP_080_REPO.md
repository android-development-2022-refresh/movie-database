---
title: Sample Data
template: main-repo.html
---

For our example application, I want to add an easy way to reset the database to contain some initial, sample data.

We start at the DAO in the `data` module, and add one-shot functions to clear the existing tables, and a concrete one-shot `resetDatabase` function to perform those clears and re-add the sample data.

Moving on to the `repository` module, we expose the `resetDatabase` function.

Finally, we move up to the `app` module, our user interface, and add a `MovieViewModel` class to manage data for the user interface. We create an instance of the repository, and expose the `Flows` to the UI along with the `resetDatabase` function. 

!!! note

    The `resetDatabase` function in the `repository` and `data` modules is a `suspend` function, so we need to start a coroutine to call it. When we're responding to user events, like button presses, we generally want to call a function in the view model and have it start its processing _using the view model's coroutine scope_, which is called `viewModelScope`. We'll see some other ways to start coroutines for looking up data when we enter a screen later.